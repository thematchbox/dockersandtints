#!/bin/bash

set -e

CMD="$1"
CLUSTER="ceph"
CONFIG_FILE="/etc/ceph/${CLUSTER}.conf"

if [ -z ${MON_IP+x} ]; then
	echo "Missing variable MON_IP."
	exit 1
fi

if [ -z ${MON_NAME+x} ]; then
	echo "Missing variable MON_NAME."
	exit 1
fi

if [ -z ${UUID+x} ]; then
	echo "Missing variable UUID."
	exit 1
fi

if [ -z ${CEPH_PUBLIC_NETWORK+x} ]; then
	echo "Missing variable CEPH_PUBLIC_NETWORK."
	exit 1
fi

if [ -e $CONFIG_FILE ]; then
	echo "Configuration is already generated."
	exit 0
fi

#
# Generate the ceph config file
#
echo "[global]
fsid = $UUID
mon initial members = "$MON_NAME"
mon host = $MON_IP
public network = $CEPH_PUBLIC_NETWORK
cluster network = $CEPH_PUBLIC_NETWORK
auth cluster required = none
auth service required = none
auth client required = none
osd journal size = 100
ms_bind_ipv6 = true
osd max object name len = 256
osd max object namespace len = 64 
osd pool default size = 2  # Write an object 4 times.
osd pool default min size = 1 # Allow writing one copy in a degraded state." > $CONFIG_FILE


case "$CMD" in
   mon)
      # Generate initial monitor map
      monmaptool --create --add $MON_NAME "${MON_IP}:6789" --fsid $UUID /etc/ceph/monmap-${CLUSTER}
      chown ceph. /etc/ceph/monmap-${CLUSTER}

      # Make the monitor directory
      mkdir -p /var/lib/ceph/mon/${CLUSTER}-${MON_NAME}
      chown ceph. /var/lib/ceph/mon/${CLUSTER}-${MON_NAME}

      # Prepare the monitor daemon's directory with the map and keyring
      ceph-mon --setuser ceph --setgroup ceph --mkfs -i ${MON_NAME} --monmap /etc/ceph/monmap-${CLUSTER} --mon-data /var/lib/ceph/mon/${CLUSTER}-${MON_NAME}

      # Create osd directory
      mkdir -p /var/lib/ceph/osd
      chown ceph. /var/lib/ceph/osd

      echo "Finished monitor bootstrap!"
      ;;
   osd)
      # Create osd directory
      mkdir -p /var/lib/ceph/osd
      chown ceph. /var/lib/ceph/osd
      echo "Finished osd bootstrap!"
      ;;
   *)
	echo "Invalid command $CMD."
	exit 1
esac
