#!/bin/bash

set -e

CMD="$1"

if [ -z ${MON_IP+x} ]; then
	echo "Missing variable MON_IP."
	exit 1
fi

if [ -z ${MON_NAME+x} ]; then
	echo "Missing variable MON_NAME."
	exit 1
fi

if [ -z ${UUID+x} ]; then
	echo "Missing variable UUID."
	exit 1
fi

if [ -z ${CLUSTER+x} ]; then
	echo "Missing variable CLUSTER."
	exit 1
fi

case "$CMD" in
   mon)
      # Generate initial monitor map
      monmaptool --create --add $MON_NAME "${MON_IP}:6789" --fsid $UUID /etc/ceph/monmap-${CLUSTER}
      chown ceph. /etc/ceph/monmap-${CLUSTER}

      # Make the monitor directory
      mkdir -p /var/lib/ceph/mon/${CLUSTER}-${MON_NAME}
      chown ceph. /var/lib/ceph/mon/${CLUSTER}-${MON_NAME}

      # Prepare the monitor daemon's directory with the map and keyring
      ceph-mon --setuser ceph --setgroup ceph --mkfs -i ${MON_NAME} --monmap /etc/ceph/monmap-${CLUSTER} --mon-data /var/lib/ceph/mon/${CLUSTER}-${MON_NAME}

      echo "Finished monitor bootstrap!"
      ;;
   osd)
      # Create osd directory
      mkdir -p /var/lib/ceph/osd
      chown ceph. /var/lib/ceph/osd
      echo "Finished osd bootstrap!"
      ;;
   *)
	echo "Invalid command $CMD."
	exit 1
esac
