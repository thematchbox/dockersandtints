#!/bin/bash

set -e

# Add elasticsearch as command if needed
if [ "${1:0:1}" = '-' ]; then
	set -- elasticsearch "$@"
fi

# Drop root privileges if we are running elasticsearch
# allow the container to be started with `--user`
if [ "$1" = 'elasticsearch' -a "$(id -u)" = '0' ]; then
	# Change the ownership of /usr/share/elasticsearch/data to elasticsearch
	chown -R elasticsearch:elasticsearch /usr/share/elasticsearch/data
	chown -R elasticsearch:elasticsearch /usr/share/elasticsearch/logs
	chown -R elasticsearch:elasticsearch /usr/share/elasticsearch/config
	
	set -- gosu elasticsearch "$@"
	#exec gosu elasticsearch "$BASH_SOURCE" "$@"
	
	# Load local plugins if any found
	if ls /ESPlugins/*.zip 1> /dev/null 2>&1; then
		plugins=`ls /ESPlugins/*.zip`
		for plugin in $plugins; do
			pluginName=`unzip -p $plugin plugin-descriptor.properties | grep -E "^name=" | cut -d'=' -f2`
			/usr/share/elasticsearch/bin/plugin remove $pluginName
			/usr/share/elasticsearch/bin/plugin install -b file:$plugin
		done
	fi
fi

# As argument is not related to elasticsearch,
# then assume that user wants to run his own process,
# for example a `bash` shell to explore this image
exec "$@"
