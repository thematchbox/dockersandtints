#/bin/bash

SCRIPT="$0"

FOLDER=`dirname $(pwd)/$SCRIPT`

docker run -d -p 9200:9200 -p 9300:9300 -v $FOLDER/config:/usr/share/elasticsearch/config -v $FOLDER/data:/usr/share/elasticsearch/data -v $FOLDER/logs:/usr/share/elasticsearch/logs thematchbox/elasticsearch
