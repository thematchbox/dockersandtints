#!/bin/bash

set -e

CLUSTER="ceph"
MON_NAME=`hostname -s`
CONFIG_FILE="/etc/ceph/${CLUSTER}.conf"

if [ -z ${MON_IP+x} ]; then
	echo "Missing variable MON_IP. Use -m or --mon-host to pass the host IP-address as a parameter."
	exit 1
fi

exec ceph-mon -d -i ${MON_NAME} --public-addr "${MON_IP}:6789" --setuser ceph --setgroup ceph
