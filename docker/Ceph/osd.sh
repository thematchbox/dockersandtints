#!/bin/bash

set -e

CLUSTER="ceph"
HOST_NAME=`hostname -s`
CONFIG_FILE="/etc/ceph/${CLUSTER}.conf"
CRUSH_LOCATION="root=default host=${HOST_NAME}"

files=`ls "/var/lib/ceph/osd"`
if [ ! -z ${files+x} ]; then
    for file in $files; do
        if [[ $file == ${CLUSTER}-* ]]; then
            if [ ! -z ${OSD_ID+x} ]; then
                echo "Multiple OSD folders found. Only one OSD supported per container."
                exit 1
            fi

            OSD_ID=${file##${CLUSTER}-}
        fi
    done
fi

if [ -z ${OSD_ID+x} ]; then
    echo "Creating new OSD"
    OSD_UUID=`uuidgen`
    OSD_ID=$(ceph osd create ${ODS_UUID} ${OSD_ID})
    if [ "$OSD_ID" -eq "$OSD_ID" ] 2>/dev/null; then
        echo "OSD created with ID: ${OSD_ID}"
    else
      echo "OSD creation failed: ${OSD_ID}"
      exit 1
    fi

    mkdir -p /var/lib/ceph/osd/${CLUSTER}-${OSD_ID}
    chown ceph. /var/lib/ceph/osd/${CLUSTER}-${OSD_ID}
    echo "created folder /var/lib/ceph/osd/${CLUSTER}-${OSD_ID}"

    # Create OSD file structure
    ceph-osd -i $OSD_ID --mkfs

    # Add the OSD to the CRUSH map
    OSD_WEIGHT=$(df -P -k /var/lib/ceph/osd/${CLUSTER}-$OSD_ID/ | tail -1 | awk '{ d= $2/1073741824 ; r = sprintf("%.2f", d); print r }')
    ceph --name=osd.${OSD_ID} osd crush create-or-move -- ${OSD_ID} ${OSD_WEIGHT} ${CRUSH_LOCATION}
else
    echo "Using ${OSD_ID}"
fi

exec ceph-osd -f -d -i ${OSD_ID}