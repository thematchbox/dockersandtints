#!/bin/sh

if [ -z "$1" ]; then
	/bin/bash
elif [ "$1" = 'mon' ]; then
	exec /mon.sh
elif [ "$1" = 'osd' ]; then
	exec /osd.sh
elif [ "$1" = 'restapi' ]; then
	exec /restapi.sh
elif [ "$1" = 'rgw' ]; then
	exec /rgw.sh
else
  $@
fi