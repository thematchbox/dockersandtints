#!/bin/bash

set -e

: ${RGW_CIVETWEB_PORT:=8080}

exec radosgw -d --rgw-frontends="civetweb port=${RGW_CIVETWEB_PORT}"
