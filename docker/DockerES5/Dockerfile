FROM centos:7
LABEL maintainer "Joeri Mesens <joeri.mesens@thematchbox.be>"

#
# All environment statements on one line
#
ENV PATH=/usr/share/elasticsearch/bin:$PATH \
	JAVA_HOME=/usr/lib/jvm/jre-1.8.0-openjdk \
	version_tag=5.2.1 \
	url_root=https://artifacts.elastic.co/downloads/elasticsearch

ENV tarball=elasticsearch-${version_tag}.tar.gz
	

RUN yum install -y java-1.8.0-openjdk-headless wget which && yum clean all

RUN groupadd -g 1000 elasticsearch && adduser -u 1000 -g 1000 -d /usr/share/elasticsearch elasticsearch

WORKDIR /usr/share/elasticsearch

# Download/extract defined ES version. busybox tar can't strip leading dir.
RUN wget --progress=bar:force $url_root/$tarball && \
    EXPECTED_SHA=$(wget -O - $url_root/$tarball.sha1) && \
    test $EXPECTED_SHA == $(sha1sum $tarball | awk '{print $1}') && \
    tar zxf $tarball && \
    chown -R elasticsearch:elasticsearch elasticsearch-$version_tag && \
    mv elasticsearch-$version_tag/* . && \
    rmdir elasticsearch-$version_tag && \
    rm $tarball

COPY docker-entrypoint.sh /

RUN set -ex && for esdirs in config data logs; do \
        mkdir -p "$esdirs"; \
        chown -R elasticsearch:elasticsearch "$esdirs"; \
    done && \
    chown elasticsearch:elasticsearch /docker-entrypoint.sh

VOLUME /usr/share/elasticsearch/data /usr/share/elasticsearch/logs /usr/share/elasticsearch/config /usr/share/elasticsearch/theMatchBox /ESPlugins

USER elasticsearch

EXPOSE 9200 9300
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["elasticsearch"]