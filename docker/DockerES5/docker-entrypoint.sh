#!/bin/bash

set -e

# Add elasticsearch as command if needed
if [ "${1:0:1}" = '-' ]; then
	set -- elasticsearch "$@"
fi

if [ "$1" = 'elasticsearch' ]; then
	# Load local plugins if any found
	if ls /ESPlugins/*.zip 1> /dev/null 2>&1; then
		plugins=`ls /ESPlugins/*.zip`
		for plugin in $plugins; do
			pluginName=`unzip -p $plugin plugin-descriptor.properties | grep -E "^name=" | cut -d'=' -f2`
			/usr/share/elasticsearch/bin/plugin remove $pluginName
			/usr/share/elasticsearch/bin/plugin install -b file:$plugin
		done
	fi
fi

# As argument is not related to elasticsearch,
# then assume that user wants to run his own process,
# for example a `bash` shell to explore this image
exec "$@"
