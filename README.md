# README #

This repository contains the docker projects and big boards tint configuration folders, maintained by theMatchBox.

# ElasticSearch #

Todo

# Ceph #

It was not straightforward to install Ceph on my BigBoards cube, since Ceph needs some bootstrapping and I have not figured out yet how to incorporate that in a tint. The Ceph docker image ceph/daemon will generate these files if not present, but the files in /etc/ceph need to the same for all monitors. This means the common files must generated first, then distributed to all nodes and then it is possible to start all Ceph monitors.

I ended up generating some files and then copying them around before installing the actual tint.

## Generate common configuration files ##

Before starting Ceph monitors, these configuration files are needed:

* ceph.conf
* ceph.mon.keyring
* ceph.client.admin.keyring
* monmap-ceph

Based on the [Ceph documentation for a manual installation](http://docs.ceph.com/docs/master/install/manual-deployment/#monitor-bootstrapping) and the scripts in the Ceph docker repo (see [config.static.sh](https://github.com/ceph/ceph-docker/blob/master/ceph-releases/jewel/ubuntu/14.04/daemon/config.static.sh) and [entrypoint.sh](https://github.com/ceph/ceph-docker/blob/master/ceph-releases/jewel/ubuntu/14.04/daemon/entrypoint.sh), I created a [script](docker/CephBootstrap/bootstrap.sh) that generates these first four files.

The script is wrapped in the Docker container [theMatchBox/ceph-bootstrap](https://cloud.docker.com/_/repository/docker/thematchbox/ceph-bootstrap). This container is used in the [Ceph-Bootstrap Tint](http://hive.bigboards.io/#/library/stack/auth0-57684c3d2e5a3718077beefc/CephBootstrap) on the BigBoards-Hive. It generates the configuration files into the persistent folder /data/ceph/bootstrap/config.

**Question: Is this the right way to do bootstrapping? Is there a better alternative?**

**Question: Since this docker container runs a script, it terminates after execution, but gets restarted 11 times. Is there a way to make it run only once?**

## Distribute the files to all cube-nodes ##

My first strategy was to put the config files in a public git repo and add map this folder as a volume into the docker container. However, at installation, all config files from git are seen as Jinja2-templates and one binary config file (the monitor map) gets corrupted at installation. Therefor I had to manually copy these files to the persistent Ceph config folder on all the nodes. As target folder I chose /data/ceph/config. I had some issues with file ownership. Some files must be owned by root, others must be owned by the Ceph user (user id 64045). I could not find an easy way to handle this, so I ended up manually transferring files through scp and fixing ownerships with chown.

** Question: Is there an easier way to distribute files to all nodes? **

## Start the Ceph monitors ##

Once the configuration files are correct and in place, starting a Ceph-monitor using docker in a Tint is easy (see tint [theMatchBoxDidItAgain](http://hive.bigboards.io/#/library/stack/auth0-57684c3d2e5a3718077beefc/theMatchBoxDidItAgain) as an example):

* Use the Ceph docker container *ceph/daemon*
* Set *--net=host*
* Use as command *mon*
* Map the config folder, */data/ceph/config:/etc/ceph*
* Map a data folder, */data/ceph/data:/var/ib/ceph*
* Add to env *CEPH_PUBLIC_NETWORK=172.17.43.0/24*
* Add to env *MON_IP={{ ansible_eth0.ipv4.address }}*

Once the tint is installed, the ceph monitors can be checked by executing *docker exec CephMonitor ceph -s*. The output should be something like:


```
#!

cluster 94f5e2aa-2277-4818-a45b-02c7f5924163
     health HEALTH_ERR
            64 pgs are stuck inactive for more than 300 seconds
            64 pgs stuck inactive
            no osds
     monmap e3: 3 mons at {smith-n1=172.17.43.1:6789/0,smith-n2=172.17.43.2:6789/0,smith-n3=172.17.43.3:6789/0}
            election epoch 6, quorum 0,1,2 smith-n1,smith-n2,smith-n3
     osdmap e1: 0 osds: 0 up, 0 in
            flags sortbitwise
      pgmap v2: 64 pgs, 1 pools, 0 bytes data, 0 objects
            0 kB used, 0 kB / 0 kB avail
                  64 creating

```

We can see that three monitors are running and they formed quorum.

## Prepare the Ceph OSD directories ##

Todo...


## Fixing Ceph stuff manually ##

If Ceph gets messed up, it is often possible to fix things manually. You can start a Ceph-shell with:

```
 docker run -it -v /data/ceph/config:/etc/ceph -v /data/ceph/data:/var/lib/ceph --net=host ceph/base bash
```